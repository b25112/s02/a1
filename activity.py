inputYear = int(input("Please input a year\n"))

if inputYear%4 == 0 and inputYear%100 != 0 or inputYear%400 == 0:
	print(f"{inputYear} is a leap year")
else:
	print(f"{inputYear} is not a leap year")

print("\n")

row = int(input("Enter number of rows\n"))
col = int(input("Enter number of columns\n"))

x=0
while x < row:
	y=0
	while y < col:
		print("*", end="")
		y += 1
	print("")
	x += 1

	

